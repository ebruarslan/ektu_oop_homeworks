#include <iostream>
#include "lifo.h"
using namespace std;

int main()
{
    lifo<int> a(10);
    lifo<int> b(5);
    int x = 5;
    int y = 5;
    a.push(x++);
    a.push(x++);
    b.push(y++);
    b.push(y++);

    lifo<int> c = a + b;

    cout << "Hello world!" << c[3] << endl;
    return 0;
}
